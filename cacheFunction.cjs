//cache function to store outputs of calls to the callback function

function cacheFunction(cb) {
    let cache = {};

    return function(argument) {
        try {
            if (cache.hasOwnProperty(argument)) {
                return cache[argument];
            } else {
                cache[argument] = cb(argument);
                return cache[argument];
            }
        } catch(error) {
            console.log(error);
        }
        
    }
}

module.exports = cacheFunction;