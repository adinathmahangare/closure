function counterFactory(){

    try {
        let counter = 0;

        return object = {
            increment : function(){
                counter++;
                return counter;
            },
            decrement : function(){
                counter--;
                return counter;
            }
        }
    } catch (error) {
        console.log(error);
    }
    
}

module.exports = counterFactory;