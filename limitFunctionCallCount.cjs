function limitFunctionCallCount(cb, n) {
    return function() {
        try {
            if (n > 0) {
                cb();
                n--;
            } else {
                return ;
            }
        } catch (error) {
            console.log(error);
        }
        
    }
}

module.exports = limitFunctionCallCount;