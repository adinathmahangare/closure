const counterFactory = require('../counterFactory.cjs');

function testCounterFactory(){
    let changeCounter = counterFactory();
    console.log(changeCounter.increment());
    console.log(changeCounter.decrement());
    console.log(changeCounter.decrement());
    console.log(changeCounter.decrement());
    console.log(changeCounter.increment());
    console.log(changeCounter.increment());
    console.log(changeCounter.increment());
}

testCounterFactory();