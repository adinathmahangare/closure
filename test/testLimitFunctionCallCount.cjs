const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function callbackFn() {
    console.log('Callback function invoked');
}

function testLimitFunctionCallCount() {
    let functionCall = limitFunctionCallCount(callbackFn, 2);

    //calling limitFunctionCallCount function 10 times
    for (let iterator = 0; iterator<10; iterator++){
        functionCall();
    }    
}

testLimitFunctionCallCount();