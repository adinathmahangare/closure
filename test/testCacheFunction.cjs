const cacheFunction = require('../cacheFunction.cjs');

function testCacheFunction() {

    //running a callback function which returns the twice valuew of a given number
    let checkCache = cacheFunction((num) => num * 2);

    console.log(checkCache(5));
    console.log(checkCache(7));
    console.log(checkCache(10));
    console.log(checkCache(4));
    console.log(checkCache(5));
    console.log(checkCache(7));
}

testCacheFunction();